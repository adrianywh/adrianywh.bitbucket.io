var derbyExchangeData

$( document ).ready(function() {
    $.getJSON('../assets/json/sosfomtDerbyExchange.json')
        .then(function (items) {
            derbyExchangeData = items
        })
        .then(function () {
            populateDerbyData();
        });
})

/**to populate pkmn selection**/
function populateDerbyData() {
    var tableHeader = "";
    var tableBody = "";

    $.each(derbyExchangeData, function (index, item) {
        if (index == 0) {
            tableHeader = "<thead><tr>";
            $.each(derbyExchangeData[index], function (key2, data) {
                if (key2 != "id"){
                    tableHeader += "<td>" + key2 + "</td>";
                }
            });
            tableHeader += "<td>Quantity</td>";
            tableHeader += "<td>Medals Needed</td>";
            tableHeader += "</tr></thead>";
            tableBody = "<tbody>";
        }
        tableBody += "<tr>";
        $.each(derbyExchangeData[index], function (key2, data) {
            if (key2 != "id"){
                tableBody += "<td id='"+key2+"-"+derbyExchangeData[index].id+"'>" + data + "</td>";
            }
        });
        tableBody += "<td><input class='input itemQtyInput' oninput='validateItemInput(id)' type='number' min=0 max=216 step=1' id='input-" + derbyExchangeData[index].id + "' value=0></td>";
        tableBody += "<td id='ticketsNeeded-" + derbyExchangeData[index].id + "'>0</td>";
        tableBody += "</tr>";
    });
    $('#exchangeTable').html(tableHeader + tableBody);
}

function validateItemInput(id){
    
    var idSelector = "#"+id;
    var value = $(idSelector).val();
    
    if ((value !== '') && (value.indexOf('.') === -1)) {
        if (id=="input-Row0" || id=="input-Row1") {
            $(idSelector).val(Math.max(Math.min(value, 1), 0));
        }else {
            $(idSelector).val(Math.max(Math.min(value, 216), 0));
        }
    } else {
        $(idSelector).val(0);
    }

    derbyExchangeCalc();
}

function validateCalcInput(id){
    
    var idSelector = "#"+id;
    var value = $(idSelector).val();
    
    if ((value !== '') && (value.indexOf('.') === -1)) {
        $(idSelector).val(Math.max(Math.min(value, 999999), 0));
    } else {
        $(idSelector).val(0);
    }

    derbyExchangeCalc();
}

function derbyExchangeCalc(){

    var itemQtyInput_arr = $(".itemQtyInput");
    var totalValue = 0;
    var totalQuantity = 0;
    var totalSalesLow = 0
    var totalSalesHigh = 0
    var slotCount = 0;

    $.each(itemQtyInput_arr, function(index, item) {
        var rowId = $(item).attr('id').split("-")[1]
        var itemQty = parseInt($(item).val());
        var itemMedalValue = parseInt(parseInt($("#Cost-"+rowId).html()));

        totalValue += (itemQty * itemMedalValue);
        totalQuantity += itemQty;
        // console.log(itemQty* parseInt($("#Cost-"+rowId).html()))

        slotCount += Math.ceil(itemQty/9);

        $("#ticketsNeeded-"+rowId).html(itemQty* parseInt($("#Cost-"+rowId).html()));

        totalSalesLow += (parseInt($("#Low-"+rowId).html())*itemQty);
        totalSalesHigh += (parseInt($("#High-"+rowId).html())*itemQty);
    });

    $("#ticketAmountSuggestion").html(Math.ceil(totalValue/parseInt($("#oddsInput").val())));
    $("#totalMedalsNeeded").html(totalValue);

    console.log("slot count: "+slotCount);

    if(totalQuantity > 216){
        $("#bagCountNotification").html("Exceeded bag limit of " +totalQuantity+ "/216");
    } else {
        $("#bagCountNotification").html("");
    }

    $("#bagSlotNotification").html("Bag slot limit " +slotCount+ "/24")

    if(totalQuantity > 216 || slotCount>24){
        $("#helpNotification").addClass("bg-danger");
    } else {
        $("#helpNotification").removeClass("bg-danger");
    }

    var goldSpend = parseInt($("#ticketQuantityInput").val())*parseInt($("#ticketValue").html());
    $("#goldSpend").html(goldSpend)
    $("#expectedTickets").html(parseInt($("#ticketQuantityInput").val())*parseInt($("#oddsInput").val()))
    
    //sales table
    $("#totalSalesLow").html(totalSalesLow)
    $("#totalSalesHigh").html(totalSalesHigh)
    $("#profitLow").html(totalSalesLow - goldSpend)
    $("#profitHigh").html(totalSalesHigh - goldSpend)

    if(goldSpend == 0){
        $("#profitPercentLow").html(0)
        $("#profitPercentHigh").html(0)
    } else {
        $("#profitPercentLow").html((((totalSalesLow - goldSpend)/goldSpend)*100).toFixed(3))
        $("#profitPercentHigh").html((((totalSalesHigh - goldSpend)/goldSpend)*100).toFixed(3))
    }

}

function resetBtForm() {
    console.log("hello")
    $('.calcInput').val(1);
    $('.itemQtyInput').val(0);
    derbyExchangeCalc();
    $('.profitTableItem').html("");
}
