var trainerData;
var pkmnData;


$( document ).ready(function() {
    $.getJSON('../assets/json/btPkmn.json')
        .then(function (items) {
            pkmnData = items
        })
        .then(function () {
            populateSelectPkmn();
        });
})

$( document ).ready(function() {
    $.getJSON('../assets/json/btTrainer.json')
        .then(function (items) {
            trainerData = items;
        })
        .then(function () {
            populateSelectTrainer()
        });
})

/********************bt helper******************** */
/**to close nav when selection made**/
$('.navbar-nav>li>a').on('click', function () {
    $('.navbar-collapse').collapse('hide');
});

/**reset button function**/
function resetBtForm() {
    populateSelectTrainer();
    populateSelectPkmn();
    $('#pkmn1Abilities').html("")
    $('#pkmn2Abilities').html("")
    $('#resultTable1').html("");
    $('#resultTable2').html("");
    $('#selectSpeedModPkmn1').val("1.0");
    $('#selectSpeedModPkmn2').val("1.0");
}

/**to populate trainer selection**/
function populateSelectTrainer() {
    var uniqueTrainerList = []
    $.each(trainerData, function (key, data) {
        var isExist = false;

        for (var i = 0; i < uniqueTrainerList.length; i++) {
            if (uniqueTrainerList[i] === key) {
                alert("duplicate trainer fround for " + key)
                break;
            }
        }

        if (!isExist) {
            uniqueTrainerList.push(key);
        }
    });

    uniqueTrainerList.sort();

    var trainerOptionHtml = "<option></<option>";
    for (var i = 0; i < uniqueTrainerList.length; i++) {
        trainerOptionHtml += "<option>" + uniqueTrainerList[i] + "</option>";
    }
    $('#selectTrainer').html(trainerOptionHtml);
}

/**to populate pkmn selection**/
function populateSelectPkmn() {
    var uniquePkmnList = [];

    $.each(pkmnData, function (key, data) {
        var isExist = false;
        var pkmnName = key.toString().substr(0, key.length - 2);

        for (var i = 0; i < uniquePkmnList.length; i++) {
            if (uniquePkmnList[i] === pkmnName) {
                isExist = true;
                break;
            }
        }
        if (!isExist) {
            uniquePkmnList.push(pkmnName);
        }
    });
    uniquePkmnList.sort();

    var PkmnOptionHtml = "<option></<option>"
    for (var i = 0; i < uniquePkmnList.length; i++) {
        PkmnOptionHtml += "<option>" + uniquePkmnList[i] + "</option>";
    }
    $('#selectPkmn1').html(PkmnOptionHtml);
    $('#selectPkmn2').html(PkmnOptionHtml);
}

$(document).on("change", "#selectTrainer", function () {

    $('#resultTable1').html("");
    $('#resultTable2').html("");
    $('#pkmn1Abilities').html("")
    $('#pkmn2Abilities').html("")
    $('#selectSpeedModPkmn1').val("1.0");
    $('#selectSpeedModPkmn2').val("1.0");

    if ($("#selectTrainer").val() != "") {
        var trainerPkmn = trainerData[$("#selectTrainer").val()]
        trainerPkmn.sort();

        var trainerPkmnOptionHtml = "<option></<option>"
        outer:
        for (var i = 0; i < trainerPkmn.length; i++) {
            for (var j=0;j<i;j++){
                if(trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) == trainerPkmn[j].substr(0, trainerPkmn[j].length - 2)){
                    continue outer
                }
            }
            trainerPkmnOptionHtml += "<option>" + trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) + "</option>"
        }
        $('#selectPkmn1').html(trainerPkmnOptionHtml);
        $('#selectPkmn2').html(trainerPkmnOptionHtml);
    } else {
        populateSelectPkmn();
    }
});

$(document).on("change", "#selectSpeedModPkmn1", function () {
    if ($("#selectPkmn1").val() == "") {
        return;
    }

    $('#resultTable1').html("");
    var count = 0;
    var tableHeader = "";
    var tableBody = "";
    var pkmnAbilities  = []

    var pkmnNameList = [];
    if ($("#selectTrainer").val() != "") {
        var trainerPkmn = trainerData[$("#selectTrainer").val()]
        trainerPkmn.sort();

        for (var i = 0; i < trainerPkmn.length; i++) {
            if(trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) == $("#selectPkmn1").val()){
                pkmnNameList.push(trainerPkmn[i])
            }
        }
    } else {
        pkmnNameList.push($("#selectPkmn1").val())
    }

    $.each(pkmnData, function (key, data) {
        var pkmnName = "";

        if ($("#selectTrainer").val() == "") {
            pkmnName = key.toString().substr(0, key.length - 2);
        } else {
            pkmnName = key;
        }

        if (pkmnNameList.includes(pkmnName)) {
            if (count == 0) {
                tableHeader = "<thead><tr>";
                $.each(pkmnData[key], function (key2, data) {
                    if (key2 == "Abilities") {
                        return true;
                    }
                    tableHeader += "<td>" + key2 + "</td>";
                });
                tableHeader += "</tr></thead>";
                tableBody = "<tbody>";
            }
            tableBody += "<tr>";
            $.each(pkmnData[key], function (key2, data) {
                if (key2 == "Speed Tier") {
                    data = Math.floor(data * $("#selectSpeedModPkmn1").val());
                }
                if (key2 == "Moves") {
                    data = data.join(", ");
                }
                if (key2 == "Abilities") {
                    for (item in data){
                        isExist = false;
                        for(item2 in pkmnAbilities){
                            if(data[item] == pkmnAbilities[item2]){
                                isExist = true
                            }
                        }
                        if (!isExist){
                            pkmnAbilities.push(data[item].trim());
                        }
                    }
                    return true;
                }
                tableBody += "<td>" + data + "</td>";
            });
            tableBody += "</tr>";
            count++;
        }
    });
    tableBody += "</tbody>";
    
    pkmnAbilities = [...new  Set(pkmnAbilities)]
    $('#pkmn1Abilities').html(pkmnAbilities.join(", "))
    $('#resultTable1').html(tableHeader + tableBody);
});

$(document).on("change", "#selectSpeedModPkmn2", function () {
    if ($("#selectPkmn2").val() == "") {
        return;
    }

    $('#resultTable2').html("");
    var count = 0;
    var tableHeader = "";
    var tableBody = "";
    var pkmnAbilities  = []

    var pkmnNameList = [];
    if ($("#selectTrainer").val() != "") {
        var trainerPkmn = trainerData[$("#selectTrainer").val()]
        trainerPkmn.sort();

        for (var i = 0; i < trainerPkmn.length; i++) {
            if(trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) == $("#selectPkmn2").val()){
                pkmnNameList.push(trainerPkmn[i])
            }
        }
    } else {
        pkmnNameList.push($("#selectPkmn2").val())
    }

    $.each(pkmnData, function (key, data) {
        var pkmnName = "";

        if ($("#selectTrainer").val() == "") {
            pkmnName = key.toString().substr(0, key.length - 2);
        } else {
            pkmnName = key;
        }

        if (pkmnNameList.includes(pkmnName)) {
            if (count == 0) {
                tableHeader = "<thead><tr>";
                $.each(pkmnData[key], function (key2, data) {
                    if (key2 == "Abilities") {
                        return true;
                    }
                    tableHeader += "<td>" + key2 + "</td>";
                });
                tableHeader += "</tr></thead>";
                tableBody = "<tbody>";
            }
            tableBody += "<tr>";
            $.each(pkmnData[key], function (key2, data) {
                if (key2 == "Speed Tier") {
                    data = Math.floor(data * $("#selectSpeedModPkmn2").val());
                }
                if (key2 == "Moves") {
                    data = data.join(", ");
                }
                if (key2 == "Abilities") {
                    for (item in data){
                        isExist = false;
                        for(item2 in pkmnAbilities){
                            if(data[item] == pkmnAbilities[item2]){
                                isExist = true
                            }
                        }
                        if (!isExist){
                            pkmnAbilities.push(data[item].trim());
                        }
                    }
                    return true;
                }
                tableBody += "<td>" + data + "</td>";
            });
            tableBody += "</tr>";
            count++;
        }
    });
    tableBody += "</tbody>";
    
    pkmnAbilities = [...new  Set(pkmnAbilities)]
    $('#pkmn2Abilities').html(pkmnAbilities.join(", "))
    $('#resultTable2').html(tableHeader + tableBody);
});

$(document).on("change", "#selectPkmn1", function () {
    $('#resultTable1').html("");
    $('#selectSpeedModPkmn1').val("1.0");
    
    // gtag('event', 'usum bt search', {
    //     'event_category':  'usum bt pkmn1',
    //     'event_label': $("#selectPkmn1").val()
    // });

    var count = 0;
    var tableHeader = "";
    var tableBody = "";
    var pkmnAbilities  = []

    var pkmnNameList = [];
    if ($("#selectTrainer").val() != "") {
        var trainerPkmn = trainerData[$("#selectTrainer").val()]
        trainerPkmn.sort();

        for (var i = 0; i < trainerPkmn.length; i++) {
            if(trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) == $("#selectPkmn1").val()){
                pkmnNameList.push(trainerPkmn[i])
            }
        }
    } else {
        pkmnNameList.push($("#selectPkmn1").val())
    }

    $.each(pkmnData, function (key, data) {
        var pkmnName = "";

        if ($("#selectTrainer").val() == "") {
            pkmnName = key.toString().substr(0, key.length - 2);
        } else {
            pkmnName = key;
        }

        if (pkmnNameList.includes(pkmnName)) {
            if (count == 0) {
                tableHeader = "<thead><tr>";
                $.each(pkmnData[key], function (key2, data) {
                    if (key2 == "Abilities") {
                        return true;
                    }
                    tableHeader += "<td>" + key2 + "</td>";
                });
                tableHeader += "</tr></thead>";
                tableBody = "<tbody>";
            }
            tableBody += "<tr>";
            $.each(pkmnData[key], function (key2, data) {
                if (key2 == "Speed Tier") {
                    data = Math.floor(data * $("#selectSpeedModPkmn1").val());
                }
                if (key2 == "Moves") {
                    data = data.join(", ");
                }
                if (key2 == "Abilities") {
                    for (item in data){
                        isExist = false;
                        for(item2 in pkmnAbilities){
                            if(data[item] == pkmnAbilities[item2]){
                                isExist = true
                            }
                        }
                        if (!isExist){
                            pkmnAbilities.push(data[item].trim());
                        }
                    }
                    return true;
                }
                tableBody += "<td>" + data + "</td>";
            });
            tableBody += "</tr>";
            count++;
        }
    });
    tableBody += "</tbody>";
    
    pkmnAbilities = [...new  Set(pkmnAbilities)]
    $('#pkmn1Abilities').html(pkmnAbilities.join(", "))
    $('#resultTable1').html(tableHeader + tableBody);
});

$(document).on("change", "#selectPkmn2", function () {
    $('#resultTable2').html("");
    $('#selectSpeedModPkmn2').val("1.0");
    
    // gtag('event', 'usum bt search', {
    //     'event_category':  'usum bt pkmn2',
    //     'event_label': $("#selectPkmn2").val()
    // });
    
    var count = 0;
    var tableHeader = "";
    var tableBody = "";
    var pkmnAbilities  = []

    var pkmnNameList = [];
    if ($("#selectTrainer").val() != "") {
        var trainerPkmn = trainerData[$("#selectTrainer").val()]
        trainerPkmn.sort();

        for (var i = 0; i < trainerPkmn.length; i++) {
            if(trainerPkmn[i].substr(0, trainerPkmn[i].length - 2) == $("#selectPkmn2").val()){
                pkmnNameList.push(trainerPkmn[i])
            }
        }
    } else {
        pkmnNameList.push($("#selectPkmn2").val())
    }

    $.each(pkmnData, function (key, data) {
        var pkmnName = "";

        if ($("#selectTrainer").val() == "") {
            pkmnName = key.toString().substr(0, key.length - 2);
        } else {
            pkmnName = key;
        }

        if (pkmnNameList.includes(pkmnName)) {
            if (count == 0) {
                tableHeader = "<thead><tr>";
                $.each(pkmnData[key], function (key2, data) {
                    if (key2 == "Abilities") {
                        return true;
                    }
                    tableHeader += "<td>" + key2 + "</td>";
                });
                tableHeader += "</tr></thead>";
                tableBody = "<tbody>";
            }
            tableBody += "<tr>";
            $.each(pkmnData[key], function (key2, data) {
                if (key2 == "Speed Tier") {
                    data = Math.floor(data * $("#selectSpeedModPkmn2").val());
                }
                if (key2 == "Moves") {
                    data = data.join(", ");
                }
                if (key2 == "Abilities") {
                    for (item in data){
                        isExist = false;
                        for(item2 in pkmnAbilities){
                            if(data[item] == pkmnAbilities[item2]){
                                isExist = true
                            }
                        }
                        if (!isExist){
                            pkmnAbilities.push(data[item].trim());
                        }
                    }
                    return true;
                }
                tableBody += "<td>" + data + "</td>";
            });
            tableBody += "</tr>";
            count++;
        }
    });
    tableBody += "</tbody>";
    
    pkmnAbilities = [...new  Set(pkmnAbilities)]
    $('#pkmn2Abilities').html(pkmnAbilities.join(", "))
    $('#resultTable2').html(tableHeader + tableBody);
});